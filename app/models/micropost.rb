class Micropost < ApplicationRecord
	#forms association with micropost belonging to only one user
	belongs_to :user
	#checks to see if character count exceeds 140 for microposts
	validates :content, length: { maximum: 140 }
end
